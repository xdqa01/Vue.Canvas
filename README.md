#Vue.Canvas
简易涂鸦工具
依赖于:html5 canvas,Vue,font-awesome
需要Internet Explorer 9+, Firefox, Opera, Chrome 以及 Safari 

文档：
-   请查看Wiki
Demo:
-   http://xdq-test.vip/vcanvas/

特点：
- 1.默认显示缩略图
-    a.可设置显示大小的百分比；
-    b.百分比大小时随页面布局变化而变化。
- 2.在单击或触压后进入编辑/预览模式；
-    a.支持指定大小的编辑区；
-    b.会悬浮在整个页面上；
-    c.允许编辑；
-    d.外置工具栏，让外观更漂亮。
- 3.资源集中
-    a.所有样式集中在js中，不需要发起额外的css 请求即可使用；
-    b.但同时，开发人员也可以使用style\css等方式为画布定义外观。

第一版：
-	Demo:
-	http://xdq-test.vip/jCanvas/
-	Code：
-	https://gitee.com/xdqa01/jQuery.Canvas

第二版：
-	Demo:
-	http://xdq-test.vip/jCanvas/
-	Code:
-	https://gitee.com/xdqa01/jQuery.Canvas2
	

Vue 版本:
-	Demo:
-	http://xdq-test.vip/vcanvas/
-	Code:
-	https://gitee.com/xdqa01/Vue.Canvas


